implicit none

integer, parameter :: mx_mol = 150, mx_sub = 300
double precision G(3, 3), Gpr(3, 3), a, b, c, al, be, ga, &
        sa, sb, sg, ca, cb, cg, V, ra(mx_mol, mx_sub), rb(mx_mol, mx_sub), &
        rc(mx_mol, mx_sub), rmax, eps, X(3, 3), W(3, 3), rabc(3, mx_mol * mx_sub), &
        CC, CCpr, tl(6, mx_mol * mx_sub), tlav(6, mx_mol), shtot, shl, &
        w1, w2, w3, tlc(3, mx_mol * mx_sub), tlcav(3, mx_mol), Vp3
double precision, parameter :: pi = atan(1.d0) * 4., pi2 = pi / 2.d0, &
        pi4 = pi / 4.d0, jpi = 1.d0 / pi, spi = sqrt(pi), A2bh = 1. / .5291772086
integer i, j, k, l, nz, ns(mx_mol), i1, i2, j1, j2, m, lmax, l1, l2, l3, mt
intrinsic derfc

read(*, *) a, b, c, al, be, ga
al = al * pi / 180.
be = be * pi / 180.
ga = ga * pi / 180.
sa = sin(al)
sb = sin(be)
sg = sin(ga)
ca = cos(al)
cb = cos(be)
cg = cos(ga)
G(1, 1) = a**2.
G(2, 2) = b**2.
G(3, 3) = c**2.
G(1, 2) = a * b * cg
G(2, 1) = G(1, 2)
G(1, 3) = a * c * cb
G(3, 1) = G(1, 3)
G(2, 3) = b * c * ca
G(3, 2) = G(2, 3)
V = dsqrt(det3(G))
Vp3 = V**(1. / 3.) * A2bh
CC = spi / V**(1.d0 / 3.d0)
CCpr = pi / CC
Gpr = inv3(G)
X(1, 1) = 1.d0 / a / sg
X(2, 1) = -cg / b / sg
X(3, 1) = 0.d0
X(1, 2) = 0.d0
X(2, 2) = 1.d0 / b
X(3, 2) = 0.d0

X(:, 3) = V / a / b / sg * Gpr(:, 3)
X = inv3(X)
W = matmul(X, Gpr)
read(*, *) nz, (ns(i), i = 1, nz)
do i = 1, nz
    do j = 1, ns(i)
        read(*, *) ra(i, j), rb(i, j), rc(i, j)
        call realfrtoA(ra(i, j), rb(i, j), rc(i, j))
    end do
end do
read(*, *) rmax, eps
lmax = rmax / min(a, b, c)
m = 0
do i1 = 1, nz
    do j1 = 1, ns(i1)
        do i2 = i1, nz
            if (i2==i1) then
                ! the same molecule
                do j2 = j1, ns(i1)
                    if (j2==j1) then
                        m = m + 1
                        TL(1, m) = 1.d0 / 3.d0
                        TL(2, m) = 1.d0 / 3.d0
                        TL(3, m) = 1.d0 / 3.d0
                        TL(4, m) = 0.d0
                        TL(5, m) = 0.d0
                        TL(6, m) = 0.d0
                        TLC(1, m) = 0.d0
                        TLC(2, m) = 0.d0
                        TLC(3, m) = 0.d0
                        rabc(1, m) = 0.d0
                        rabc(2, m) = 0.d0
                        rabc(3, m) = 0.d0
                    else
                        !different submolecules in one molecule
                        m = m + 1
                        rabc(1, m) = ra(i2, j2) - ra(i1, j1)
                        rabc(2, m) = rb(i2, j2) - rb(i1, j1)
                        rabc(3, m) = rc(i2, j2) - rc(i1, j1)
                        TL(:, m) = Hten0(rabc(1, m), rabc(2, m), rabc(3, m))
                    end if
                end do
            else
                !different molecules
                do j2 = 1, ns(i2)
                    m = m + 1
                    rabc(1, m) = ra(i2, j2) - ra(i1, j1)
                    rabc(2, m) = rb(i2, j2) - rb(i1, j1)
                    rabc(3, m) = rc(i2, j2) - rc(i1, j1)
                    TL(:, m) = Hten(rabc(1, m), rabc(2, m), rabc(3, m))
                end do
            end if
        end do
    end do
end do
mt = m
shtot = sum(abs(TL))
do l = 1, lmax
    shl = 0.d0
    l1 = l
    do m = 1, mt
        do l2 = -l, l
            do l3 = -l, l
                call calcTL(l1, l2, l3)
            end do
        end do
        l2 = l
        do l1 = -l + 1, l - 1
            do l3 = -l, l
                call calcTL(l1, l2, l3)
            end do
        end do
        l3 = l
        do l1 = -l + 1, l - 1
            do l2 = -l + 1, l - 1
                call calcTL(l1, l2, l3)
            end do
        end do
    end do
    shtot = shtot + shl
    if (abs(shl / shtot)<eps) then
        exit
    endif
end do
write(*, *) "THE LORENTZ-FACTOR-TENSOR"
if (l<lmax) then
    write(*, *) "convergence at ", l, "th shell, requested lshell max:", lmax
else
    write(*, *) "results not fully converged, increase R_cut"
endif
write(*, 400) TL(:, :mt)

400   format(3x, 6e20.12e2)
500   format(3x, 3e20.12e2)

contains

    function det3(a)
        double precision a(3, 3), det3
        det3 = 0.d0
        det3 = a(1, 1) * a(2, 2) * a(3, 3) + &
                a(1, 2) * a(2, 3) * a(3, 1) + &
                a(1, 3) * a(2, 1) * a(3, 2) - &
                a(1, 1) * a(2, 3) * a(3, 2) - &
                a(1, 2) * a(2, 1) * a(3, 3) - &
                a(1, 3) * a(2, 2) * a(3, 1)
    end function

    function inv3(a)
        double precision a(3, 3), inv3(3, 3), detm1
        detm1 = 1.d0 / det3(a)
        inv3(1, 1) = detm1 * (a(2, 2) * a(3, 3) - a(2, 3) * a(3, 2))
        inv3(1, 2) = detm1 * (a(1, 3) * a(3, 2) - a(1, 2) * a(3, 3))
        inv3(1, 3) = detm1 * (a(1, 2) * a(2, 3) - a(1, 3) * a(2, 2))
        inv3(2, 1) = detm1 * (a(2, 3) * a(3, 1) - a(2, 1) * a(3, 3))
        inv3(2, 2) = detm1 * (a(1, 1) * a(3, 3) - a(1, 3) * a(3, 1))
        inv3(2, 3) = detm1 * (a(1, 3) * a(2, 1) - a(1, 1) * a(2, 3))
        inv3(3, 1) = detm1 * (a(2, 1) * a(3, 2) - a(2, 2) * a(3, 1))
        inv3(3, 2) = detm1 * (a(1, 2) * a(3, 1) - a(1, 1) * a(3, 2))
        inv3(3, 3) = detm1 * (a(1, 1) * a(2, 2) - a(1, 2) * a(2, 1))
    end function

    subroutine realfrtoA(xx, yy, zz)
        double precision xx, yy, zz, a, b, c
        a = xx * CC
        b = yy * CC
        c = zz * CC
        xx = X(1, 1) * a + X(1, 2) * b + X(1, 3) * c
        yy = X(2, 1) * a + X(2, 2) * b + X(2, 3) * c
        zz = X(3, 1) * a + X(3, 2) * b + X(3, 3) * c
    end subroutine

    subroutine recifrtoA(xx, yy, zz)
        double precision xx, yy, zz, a, b, c
        a = xx * CCpr
        b = yy * CCpr
        c = zz * CCpr
        xx = W(1, 1) * a + W(1, 2) * b + W(1, 3) * c
        yy = W(2, 1) * a + W(2, 2) * b + W(2, 3) * c
        zz = W(3, 1) * a + W(3, 2) * b + W(3, 3) * c
    end subroutine

    function Gten(q1, q2, q3)
        double precision Gten(6), qxx, qyy, qzz, qxy, qxz, qyz, qsq, q1, q2, q3
        qxx = q1**2.d0
        qyy = q2**2.d0
        qzz = q3**2.d0
        qxy = q1 * q2
        qxz = q1 * q3
        qyz = q2 * q3
        qsq = qxx + qyy + qzz
        if (qsq>290.d0) then
            Gten = (/0.d0, 0.d0, 0.d0, 0.d0, 0.d0, 0.d0/)
        else
            qsq = dexp(-qsq) / qsq
            Gten(1) = qxx * qsq
            Gten(2) = qyy * qsq
            Gten(3) = qzz * qsq
            Gten(4) = qxy * qsq
            Gten(5) = qxz * qsq
            Gten(6) = qyz * qsq
        endif
    end function

    function Hten0(rx, ry, rz)
        double precision Hten0(6), rxx, ryy, rzz, rxy, rxz, ryz, rsq, ersq, erfr, rx, ry, rz
        rxx = rx**2.
        ryy = ry**2.
        rzz = rz**2.
        rxy = rx * ry
        rxz = rx * rz
        ryz = ry * rz
        rsq = rxx + ryy + rzz
        ersq = dexp(-rsq)
        erfr = (1.d0 - derfc(sqrt(rsq)))
        Hten0(1) = (1.d0 - 3.d0 * rxx / rsq) * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * rxx / rsq + 2.d0 * rxx)
        Hten0(2) = (1.d0 - 3.d0 * ryy / rsq) * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * ryy / rsq + 2.d0 * ryy)
        Hten0(3) = (1.d0 - 3.d0 * rzz / rsq) * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * rzz / rsq + 2.d0 * rzz)
        Hten0(4) = -3.d0 * rxy / rsq * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * (3.d0 * rxy / rsq + 2.d0 * rxy)
        Hten0(5) = -3.d0 * rxz / rsq * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * (3.d0 * rxz / rsq + 2.d0 * rxz)
        Hten0(6) = -3.d0 * ryz / rsq * erfr / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * (3.d0 * ryz / rsq + 2.d0 * ryz)
        Hten0 = Hten0 * spi / 4.d0
    end function

    function Hten(rx, ry, rz)
        double precision Hten(6), rxx, ryy, rzz, rxy, rxz, ryz, rsq, ersq, erfr, rx, ry, rz
        rxx = rx**2.
        ryy = ry**2.
        rzz = rz**2.
        rxy = rx * ry
        rxz = rx * rz
        ryz = ry * rz
        rsq = rxx + ryy + rzz
        ersq = dexp(-rsq)
        erfr = (1.d0 - derfc(sqrt(rsq)))
        Hten(1) = -(1.d0 - 3.d0 * rxx / rsq) * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * rxx / rsq + 2.d0 * rxx)
        Hten(2) = -(1.d0 - 3.d0 * ryy / rsq) * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * ryy / rsq + 2.d0 * ryy)
        Hten(3) = -(1.d0 - 3.d0 * rzz / rsq) * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq * &
                (-1.d0 + 3.d0 * rzz / rsq + 2.d0 * rzz)
        Hten(4) = 3.d0 * rxy / rsq * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq *&
                (3.d0 * rxy / rsq + 2.d0 * rxy)
        Hten(5) = 3.d0 * rxz / rsq * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq *&
                (3.d0 * rxz / rsq + 2.d0 * rxz)
        Hten(6) = 3.d0 * ryz / rsq * (1.d0 - erfr) / rsq**(3. / 2.) + 2.d0 / spi / rsq * ersq *&
                (3.d0 * ryz / rsq + 2.d0 * ryz)
        Hten = Hten * spi / 4.d0
    end function

    subroutine calcTL(l1, l2, l3)
        double precision rr(3), tmp(6), q1, q2, q3, ph, ll1, ll2, ll3
        integer l1, l2, l3
        ll1 = dble(l1)
        ll2 = dble(l2)
        ll3 = dble(l3)
        q1 = ll1
        q2 = ll2
        q3 = ll3
        call realfrtoA(ll1, ll2, ll3)
        call recifrtoA(q1, q2, q3)
        rr(1) = rabc(1, m) + ll1
        rr(2) = rabc(2, m) + ll2
        rr(3) = rabc(3, m) + ll3
        ph = 2.d0 * (rabc(1, m) * q1 + rabc(2, m) * q2 + rabc(3, m) * q3)
        tmp = Hten(rr(1), rr(2), rr(3)) - Gten(q1, q2, q3) * dcos(ph)
        rr(1) = rabc(1, m) - ll1
        rr(2) = rabc(2, m) - ll2
        rr(3) = rabc(3, m) - ll3
        tmp = tmp + Hten(rr(1), rr(2), rr(3)) - Gten(-q1, -q2, -q3) * dcos(ph)
        shl = shl + sum(abs(tmp))
        TL(:, m) = TL(:, m) + tmp
    end subroutine

end
