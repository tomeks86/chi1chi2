implicit none

integer, parameter :: mx_op = 30, mx_at1 = 100, mx_at2 = 100, n_cz = 10000000, mx_pts = 1000000
double precision G(3, 3), Gpr(3, 3), a, b, c, al, be, ga, sa, sb, sg, ca, cb, cg, &
        V, X(3, 3), W(3, 3), eps(3, 3), xx, yy, zz, l123(3), r_cut, &
        coor_xyz(mx_op * mx_at1, 3), coor_xyzA(mx_op * mx_at1, 3), &
        X_inv(3, 3), xyz(3), RR(3), charg(mx_op * mx_at1), &
        rot(mx_op, 3, 3), tr(mx_op, 3), u(3), R0(3), RR0(n_cz, 3), RRt(3), RRt1(3), &
        Q, F(3), FF(n_cz, 3), F_av(3), pts(mx_pts, 4), chg_all(4 * n_cz, 4)
double precision, parameter :: pi = atan(1.d0) * 4., &
        A2bh = 1.88972613288564306722
character(len = 2) :: at_names(mx_op * mx_at1)
integer i, j, k, l, m, n, n_at1, n_at2, n_op, l_max, cz_nuc1, cz_nuc2, &
        q_at(mx_op * mx_at1), l1, l2, l3, n_pts, n_chg

read(*, *) a, b, c, al, be, ga
al = al * pi / 180.
be = be * pi / 180.
ga = ga * pi / 180.
sa = sin(al)
sb = sin(be)
sg = sin(ga)
ca = cos(al)
cb = cos(be)
cg = cos(ga)
G(1, 1) = a**2.
G(2, 2) = b**2.
G(3, 3) = c**2.
G(1, 2) = a * b * cg
G(2, 1) = G(1, 2)
G(1, 3) = a * c * cb
G(3, 1) = G(1, 3)
G(2, 3) = b * c * ca
G(3, 2) = G(2, 3)
V = dsqrt(det3(G))
Gpr = inv3(G)
!A,B',C*
X(1, 1) = 1.d0 / a
X(2, 1) = 0.d0
X(3, 1) = 0.d0
X(1, 2) = -cg / a / sg
X(2, 2) = 1.d0 / b / sg
X(2, 3) = 0.d0
X(:, 3) = V / a / b / sg * Gpr(:, 3)
X_inv = X
X = inv3(X)
W = matmul(X, Gpr)
read(*, *) r_cut, n_op
read(*, *) n_at1, n_at2
do i = 1, n_at1
    read(*, *) q_at(i), coor_xyz(i, :3), charg(i)
    coor_xyzA(i, :) = coor_xyz(i, :)
    call realfrtoA(coor_xyzA(i, :))
end do
cz_nuc1 = 0
cz_nuc2 = 0
do i = 1, n_at2
    cz_nuc1 = cz_nuc1 + q_at(i)
end do
if (n_at2/=n_at1) then
    do i = n_at2 + 1, n_at1
        cz_nuc2 = cz_nuc2 + q_at(i)
    end do
endif
do i = 1, n_op
    do j = 1, 3
        read(*, *) (rot(i, j, k), k = 1, 3), tr(i, j)
    end do
end do
l_max = R_cut / min(a, b, c) + 2
!write(*,*) l_max
open(11, file = "molecule.dat")
open(12, file = "charge.dat")
R0 = (/0., 0., 0./)
do i = 1, n_at2
    R0(1) = R0(1) + coor_xyzA(i, 1) * q_at(i)
    R0(2) = R0(2) + coor_xyzA(i, 2) * q_at(i)
    R0(3) = R0(3) + coor_xyzA(i, 3) * q_at(i)
    write(11, '(i4,3(" ",f20.15))') q_at(i), coor_xyzA(i, 1), coor_xyzA(i, 2), coor_xyzA(i, 3)    !,charg(i)
end do
R0 = R0 / cz_nuc1
!do i=1,n_at1
!	coor_xyzA(i,:)=coor_xyzA(i,:)-R0
!end do
!write(*,*) R0
Q = 0.d0
F = (/0.d0, 0.d0, 0.d0/)
n_chg = 0
if (n_at1>n_at2) then    !l1,l2,l3=(0,0,0)
    do i = n_at1 + 1, n_at2
        !write(11, '(i4,3(" ",f20.15))') q_at(i), coor_xyzA(i, 1), coor_xyzA(i, 2), coor_xyzA(i, 3)
        !write(11,'(i4,3(" ",f20.15))') 0,coor_xyzA(i,1),coor_xyzA(i,2),coor_xyzA(i,3)
        write(12, '(3(f20.15," "))') coor_xyzA(i, 1), coor_xyzA(i, 2), coor_xyzA(i, 3)    !,charg(i)
        n_chg = n_chg + 1
        chg_all(n_chg, :3) = coor_xyzA(i, :)
        chg_all(n_chg, 4) = charg(i)
        RR = (R0 - coor_xyzA(i, :)) * A2bh
        F = F + charg(i) * RR / norm(RR)**3.
        do n = 1, n_at2
            RRt = (coor_xyzA(n, :) - RR) * A2bh
            FF(n, :) = FF(n, :) + charg(j) * RRt / norm(RRt)**3.
        end do
    end do
endif
do m = 1, n_op
    do j = 1, n_at1
        xyz(1) = coor_xyz(j, 1)
        xyz(2) = coor_xyz(j, 2)
        xyz(3) = coor_xyz(j, 3)
        call transf_fr(xyz, rot(m, :, :), tr(m, :))
        call realfrtoA(xyz)
        xyz = xyz
        do l1 = -l_max, l_max
            do l2 = -l_max, l_max
                do l3 = -l_max, l_max
                    if ((m==1).and.(l1==0).and.(l2==0).and.(l3==0).and.(j<=n_at2)) then
                        !write(*,*) q_at(j),RR
                        cycle
                    else
                        l123(1) = l1
                        l123(2) = l2
                        l123(3) = l3
                        call realfrtoA(l123)
                        RR = xyz + l123
                        RRt = RR - R0!coor_xyzA(k,:)
                        if (norm(RRt)<R_cut) then
                            !write(11, '(i4,4(" ",f30.15))') q_at(j), RR(1), RR(2), RR(3), charg(j)
                            !write(11,'(i4,3(" ",f30.15))') 0,RR(1),RR(2),RR(3)
                            write(12, '(4(f20.15," "))') RR(1), RR(2), RR(3), charg(j)
                            n_chg = n_chg + 1
                            chg_all(n_chg, :3) = RR
                            chg_all(n_chg, 4) = charg(j)
                            Q = Q + charg(j)
                            do n = 1, n_at2
                                RRt = (coor_xyzA(n, :) - RR) * A2bh
                                FF(n, :) = FF(n, :) + charg(j) * RRt / norm(RRt)**3.
                            end do
                            RR = (R0 - RR) * A2bh
                            F = F + charg(j) * RR / norm(RR)**3.
                        endif
                    endif
                end do
            end do
        end do
    end do
end do

write(*, '(a,3f10.2)') "R cut (A): ", R_cut
write(*, '(a,3f10.5)') "total charge: ", Q
write(*, '(a,3(3f10.5))') "electric field at the center (GV/m):", F * 514.21

close(11)
close(12)

contains

    subroutine normalize(u)
        double precision :: u(3), sm
        sm = 0.d0
        do i = 1, 3
            sm = sm + u(i)**2.
        end do
        sm = dsqrt(sm)
        do i = 1, 3
            u(i) = u(i) / sm
        end do
    end subroutine

    function norm(u)
        double precision u(3), norm
        norm = 0.d0
        do i = 1, 3
            norm = norm + u(i)**2.
        end do
        norm = dsqrt(norm)
    end function

    function det3(a)
        double precision a(3, 3), det3
        det3 = 0.d0
        det3 = a(1, 1) * a(2, 2) * a(3, 3) + &
                a(1, 2) * a(2, 3) * a(3, 1) + &
                a(1, 3) * a(2, 1) * a(3, 2) - &
                a(1, 1) * a(2, 3) * a(3, 2) - &
                a(1, 2) * a(2, 1) * a(3, 3) - &
                a(1, 3) * a(2, 2) * a(3, 1)
    end function

    function inv3(a)
        double precision a(3, 3), inv3(3, 3), detm1
        detm1 = 1.d0 / det3(a)
        inv3(1, 1) = detm1 * (a(2, 2) * a(3, 3) - a(2, 3) * a(3, 2))
        inv3(1, 2) = detm1 * (a(1, 3) * a(3, 2) - a(1, 2) * a(3, 3))
        inv3(1, 3) = detm1 * (a(1, 2) * a(2, 3) - a(1, 3) * a(2, 2))
        inv3(2, 1) = detm1 * (a(2, 3) * a(3, 1) - a(2, 1) * a(3, 3))
        inv3(2, 2) = detm1 * (a(1, 1) * a(3, 3) - a(1, 3) * a(3, 1))
        inv3(2, 3) = detm1 * (a(1, 3) * a(2, 1) - a(1, 1) * a(2, 3))
        inv3(3, 1) = detm1 * (a(2, 1) * a(3, 2) - a(2, 2) * a(3, 1))
        inv3(3, 2) = detm1 * (a(1, 2) * a(3, 1) - a(1, 1) * a(3, 2))
        inv3(3, 3) = detm1 * (a(1, 1) * a(2, 2) - a(1, 2) * a(2, 1))
    end function

    subroutine realfrtoA(xyz)
        double precision xyz(3), xyz2(3)
        xyz2(1) = xyz(1)
        xyz2(2) = xyz(2)
        xyz2(3) = xyz(3)
        xyz(1) = X(1, 1) * xyz2(1) + X(1, 2) * xyz2(2) + X(1, 3) * xyz2(3)
        xyz(2) = X(2, 1) * xyz2(1) + X(2, 2) * xyz2(2) + X(2, 3) * xyz2(3)
        xyz(3) = X(3, 1) * xyz2(1) + X(3, 2) * xyz2(2) + X(3, 3) * xyz2(3)
    end subroutine

    subroutine Atorealfr(xyz)
        double precision xyz(3), xyz2(3)
        xyz2(1) = xyz(1)
        xyz2(2) = xyz(2)
        xyz2(3) = xyz(3)
        xyz(1) = X_inv(1, 1) * xyz2(1) + X_inv(1, 2) * xyz2(2) + X_inv(1, 3) * xyz2(3)
        xyz(2) = X_inv(2, 1) * xyz2(1) + X_inv(2, 2) * xyz2(2) + X_inv(2, 3) * xyz2(3)
        xyz(3) = X_inv(3, 1) * xyz2(1) + X_inv(3, 2) * xyz2(2) + X_inv(3, 3) * xyz2(3)
    end subroutine

    subroutine recifrtoA(xyz)
        double precision xyz(3), xyz2(3)
        xyz2(1) = xyz(1)
        xyz2(2) = xyz(2)
        xyz2(3) = xyz(3)
        xyz(1) = W(1, 1) * xyz2(1) + W(1, 2) * xyz2(2) + W(1, 3) * xyz2(3)
        xyz(2) = W(2, 1) * xyz2(1) + W(2, 2) * xyz2(2) + W(2, 3) * xyz2(3)
        xyz(3) = W(3, 1) * xyz2(1) + W(3, 2) * xyz2(2) + W(3, 3) * xyz2(3)
    end subroutine

    subroutine transf_fr(xyz, rot, tr)
        double precision xyz(3), rot(3, 3), tr(3)
        xyz = matmul(rot, xyz) + tr
    end subroutine

end program
