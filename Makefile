programs = lorentz charge_generator

fortrandir = fortran
srcs = $(addprefix $(fortrandir)/, $(programs:=.f90))

builddir = build
targs = $(addprefix $(builddir)/, $(programs))

FC = gfortran

ifndef VERBOSE
.SILENT:
endif

.PHONY: clean

all: $(builddir) $(targs)
	echo "compiled programs in the build/ directory"
	echo "consider adding the build/ directory to your PATH"

$(builddir):
	mkdir -p $@

$(builddir)/%: $(fortrandir)/%.f90
	$(FC) -o $@ $^

clean:
	rm -rf $(builddir)
